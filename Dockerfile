FROM ubuntu:20.04

RUN apt-get update -y
RUN apt-get upgrade -y

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get install -y build-essential libtool libssl-dev curl zlib1g-dev mandoc python3-pip groff txt2html
RUN pip install rst2html markdown pygments

RUN mkdir /src
COPY ./cgit /src/cgit

RUN cd /src/cgit && make get-git && make && make install

COPY ./cgitrc /etc/cgitrc
COPY ./custom.css /var/www/htdocs/cgit/custom.css


RUN apt-get install -y apache2
COPY ./cgit.conf /etc/apache2/sites-available/cgit.conf

RUN a2enmod rewrite && a2enmod cgi \
    && cd /etc/apache2/mods-enabled \
    && ln -s ../mods-available/cgi.load cgi.load \
    && rm /etc/apache2/sites-enabled/000-default.conf \
    && ln -s /etc/apache2/sites-available/cgit.conf /etc/apache2/sites-enabled/

RUN rm -r /src/cgit

RUN mkdir /repos

RUN apt-get install -y supervisor
COPY ./supervisord.conf /etc/supervisord.conf

RUN mkdir /filters
COPY ./filters /filters

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
