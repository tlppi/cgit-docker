# cgit-docker
## A dockerised verion of cgit
It's very opinionated. 

# usage
- Put your verson of cgit into ./cgit/ (the Dockerfile will run `make get-git` so dont worry about the rest of that).
- Run `docker build . -t cgit` to build the docker container
- Edit the `docker-compose.yml` to your liking. I use jwilder's nginx-proxy container so that's how i have it setup. Obviously change the domain. its mine by default because its my project. cry about it.
- RUN THIS BEHIND A REVERSE PROXY. I HAVEN'T DONE SSL. I HAVEN'T SETUP DOMAINS. IF YOU WANT TO USE IT AS-IS, USE A REVERSE PROXY. tyvm
- Create a `./repos/` directory
- Start the docker file (`docker-compose up -d`)

# creating repos
Create a BARE repo by doing:

```bash
mkdir ./repos/REPONAME.git
cd ./repos/REPONAME.git
git init --bare

```

## READMEs
These formats are supported:

- manpage (must be `README.7`)
- markdown (must be `README.md`)
- plaintext (must be `README`)

Then push to that.
